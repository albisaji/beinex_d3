import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Data } from '@angular/router';
import * as d3 from 'd3';
import { STACKED_DATA_1 } from 'src/app/config/graph.config';

@Component({
  selector: 'app-stacked-bar',
  templateUrl: './stacked-bar.component.html',
  styleUrls: ['./stacked-bar.component.css']
})
export class StackedBarComponent implements OnInit,OnChanges {
  @Input() data:any

  constructor() { }
  ngOnChanges(changes: SimpleChanges): void {
    const parent_div:any = document.getElementById('chart');
    parent_div.setAttribute('id', this.data.id);
    console.log(parent_div)
    this.drawStackedBarGraph();
  }

  ngOnInit() {
  }
  
  drawStackedBarGraph():void{
    const colors=['#32ac71','#e29c33','#c13148','#265c66']
    // const data:any =data
    const margin = { top: 10, right: 10, bottom: 20, left: 40 };

    const width = 450 - margin.left - margin.right;
    const height = 420 - margin.top - margin.bottom;

    const svg = d3.select(`#${this.data.id}`)
      .append('svg')
        .attr('width', width + margin.left + margin.right)
        .attr('height', height + margin.top + margin.bottom)
      .append('g')
        .attr('transform', `translate(${margin.left},${margin.top})`);


console.log(`#${this.data.id}`)
    const fruit:any = Object.keys(this.data.graphData[0]).filter(d => d != "month");
    const months = this.data.graphData.map((d:any) => d.month);

    const stackedData:any = d3.stack()
        .keys(fruit)(this.data.graphData);

    const xMax:any = d3.max(stackedData[stackedData.length - 1], (d:any) => d[1]);


    const x = d3.scaleLinear()
        .domain([0, xMax]).nice()
        .range([0, width]);

    const y :any= d3.scaleBand()
        .domain(months)
        .range([0, height])
        .padding(0.25);

    const xAxis = d3.axisBottom(x).ticks(5, '~s');
    const yAxis = d3.axisLeft(y);

    svg.append('g')
        .attr('transform', `translate(0,${height})`)
        .attr("strke-opacity",0)
        .classed('x axis',true)
        .attr("color",'#fff')
        .style("font-size",'12px')
        .style("font-family",'Montserrat')
        .call(xAxis)
        .call((g:any) => g.select('.domain').remove());

    svg.append("g")
    .attr("strke-opacity",0)
    .classed('y axis',true)
    .attr("color",'7c828A')
    .style("font-size",'12px')
    .style("font-family",'Montserrat')
        .call(yAxis)
        .call(g => g.select('.domain').remove());

    const layers = svg.append('g')
      .selectAll('g')
      .data(stackedData)
      .join('g')
        .attr('fill', (d:any,i:any) =>{return colors[i]});

    const duration = 1000;
    const t = d3.transition()
        .duration(duration)
        .ease(d3.easeLinear);

    layers.each(function(_, i) {
      d3.select(this)
        .selectAll('rect')
        .data((d:any) => d)
        .join('rect')
          .attr('x', (d:any) => x(d[0]))
          .attr('y', (d:any) => y(d.data.month))
          .attr('height', y.bandwidth())
        .transition(t)
          .delay(i * duration)
          .attr('width', (d:any) => x(d[1]) - x(d[0]));
    });
    layers.selectAll("text")
    .data((d:any)=>{
      return d
    })
    .enter()
    .append("text")
    .attr("class","text")
    .style("fill",'#fff')
    .style("font-size",'12px')
    .style("font-family",'Montserrat')
    .style("text-align",'center')
    .attr("x",(d:any)=>x(d[0]))
    .attr("y",(d:any)=>y(d.data.month)+25)
    .text((d:any)=>{
      return d[1];
    })
  }

}
